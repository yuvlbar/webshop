from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models


class Coupon(models.Model):
    """Модель купон"""
    code = models.CharField(max_length=50, unique=True, verbose_name='код купона')
    valid_from = models.DateTimeField(verbose_name='начало действия купона')
    valid_to = models.DateTimeField(verbose_name='окончание действия купона')
    discount = models.IntegerField(validators=[MinValueValidator(0), MaxValueValidator(100)],
                                   verbose_name='величина скидки')
    active = models.BooleanField()

    class Meta:
        verbose_name = 'Купон'
        verbose_name_plural = 'Купоны'

    def __str__(self):
        return self.code
