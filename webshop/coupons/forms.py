from django import forms


class CouponApplyForm(forms.Form):
    """Форма введения кода купона"""
    code = forms.CharField(label='Код')
