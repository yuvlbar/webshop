from __future__ import absolute_import, unicode_literals

from celery import shared_task
from django.core.mail import send_mail
from .models import Order

@shared_task
def order_created(order_id):
    """Задача отправки сообщения при успешном выполнении заказа"""
    order = Order.objects.get(id=order_id)
    subject = 'Заказ номер {}'.format(order.id)
    message = 'Уважаемый {},\n\nВы успешно разместили заказ.\
                  Номер заказа {}.'.format(order.first_name,
                                            order.id)
    mail_sent = send_mail(subject,
                          message,
                          'admin@webshop.com',
                          [order.email])
    return mail_sent
