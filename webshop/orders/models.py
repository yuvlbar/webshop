from decimal import Decimal

from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models

from coupons.models import Coupon
from shop.models import Product


class Order(models.Model):
    """Модель заказа"""
    first_name = models.CharField(max_length=50, verbose_name='Имя')
    last_name = models.CharField(max_length=50, verbose_name='Фамилия')
    email = models.EmailField()
    address = models.CharField(max_length=250, verbose_name='Адрес')
    postal_code = models.CharField(max_length=20, verbose_name='Почтовый код')
    city = models.CharField(max_length=100, verbose_name='Город')
    created = models.DateTimeField(auto_now_add=True, verbose_name='Дата создания заказа')
    updated = models.DateTimeField(auto_now=True, verbose_name='Дата редактирования заказа')
    paid = models.BooleanField(default=False, verbose_name='Оплачено')
    coupon = models.ForeignKey(Coupon, on_delete=models.SET_NULL,
                               null=True, blank=True,
                               related_name='orders',
                               verbose_name='купон')
    discount = models.IntegerField(default=0, validators=[MinValueValidator(0),
                                                          MaxValueValidator(100)],
                                   verbose_name='скидка')

    def get_total_cost(self):
        """Получить общую стоимость заказа"""
        total_coast = sum(item.get_cost() for item in self.items.all())
        return total_coast - total_coast * (self.discount / Decimal('100'))

    class Meta:
        ordering = ('-created', )
        verbose_name = 'Заказ'
        verbose_name_plural = 'Заказы'

    def __str__(self):
        return 'Заказ {}'.format(self.id)


class OrderItem(models.Model):
    """Модель для связи товара с заказом"""
    order = models.ForeignKey(Order,
                              on_delete=models.CASCADE,
                              related_name='items',
                              verbose_name='Заказ')
    product = models.ForeignKey(Product,
                                on_delete=models.CASCADE,
                                related_name='order_items',
                                verbose_name='Товары в заказе')
    price = models.DecimalField(max_digits=10, decimal_places=2, verbose_name='Цена')
    quantity = models.PositiveIntegerField(default=1, verbose_name='Кол-во')

    def get_cost(self):
        """ Получить общую стоимость товара в заказе"""
        return self.price * self.quantity

    class Meta():
        verbose_name = 'Цена и количество товара в заказе'
        verbose_name_plural = 'Цены и количество товара в заказе'

    def __str__(self):
        return '{}'.format(self.id)
