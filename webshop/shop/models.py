from django.db import models
from django.urls import reverse


class Category(models.Model):
    """Модель категорий товаров"""
    name = models.CharField(max_length=200,
                            db_index=True,
                            verbose_name='Название категории')
    slug = models.SlugField(max_length=200,
                            unique=True)

    def get_absolute_url(self):
        return reverse('shop:product_list_by_category', args=[self.slug])

    class Meta:
        ordering = ('name',)
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'

    def __str__(self):
        return self.name


class Product(models.Model):
    """Модель товаров"""
    category = models.ForeignKey(Category, on_delete=models.CASCADE,
                                 related_name='products',
                                 verbose_name='Категории')
    name = models.CharField(max_length=200,
                            db_index=True,
                            verbose_name='Название товара')
    slug = models.SlugField(max_length=200,
                            db_index=True)
    image = models.ImageField(upload_to='products/%Y/%m/%d',
                              blank=True,
                              verbose_name='Изображение товара')
    description = models.TextField(blank=True,
                                   verbose_name='Описание товара')
    price = models.DecimalField(max_digits=10,
                                decimal_places=2,
                                verbose_name='Цена товара')
    available = models.BooleanField(default=True,
                                    verbose_name='В наличии')
    created = models.DateTimeField(auto_now_add=True,
                                   verbose_name='Дата создания')
    updated = models.DateTimeField(auto_now=True,
                                   verbose_name='Дата изменения')

    def get_absolute_url(self):
        return reverse('shop:product_detail', args=[self.id, self.slug])

    class Meta:
        ordering = ('name',)
        index_together = (('id', 'slug'),)
        verbose_name = 'Товар'
        verbose_name_plural = 'Товары'

    def __str__(self):
        return self.name
