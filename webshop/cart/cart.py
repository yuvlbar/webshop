from decimal import Decimal

from django.conf import settings

from coupons.models import Coupon
from shop.models import Product


class Cart(object):
    """Класс для работы с корзиной товаров"""

    def __init__(self, request):
        """Инициализация объекта корзины"""
        self.session = request.session  # текущая сессия записывается в атрибут
        cart = self.session.get(settings.CART_SESSION_ID)  # получение данных корзины в текущей сессии
        if not cart:  # если их нет
            cart = self.session[settings.CART_SESSION_ID] = {}  # создаем пустой словарь
        self.cart = cart  # словарь с данными корзины записываем в атрибут
        self.coupon_id = self.session.get('coupon_id')#инициализация объекта купона из сессии

    def __iter__(self):
        """Итерация по товарам корзины"""
        product_ids = self.cart.keys()
        products = Product.objects.filter(id__in=product_ids)
        cart = self.cart.copy()
        for product in products:
            cart[str(product.id)]['product'] = product

        for item in cart.values():
            item['price'] = Decimal(item['price'])
            item['total_price'] = item['price'] * item['quantity']
            yield item

    def __len__(self):
        """Возвращает общее количество товаров в корзине"""
        return sum(item['quantity'] for item in self.cart.values())

    def add(self, product, quantity=1, update_quantity=False):
        """Добавление товара в корзину"""
        product_id = str(product.id)
        if product_id not in self.cart:
            self.cart[product_id] = {'quantity': 0,
                                     'price': str(product.price)}
        if update_quantity:
            self.cart[product_id]['quantity'] = quantity
        else:
            self.cart[product_id]['quantity'] += quantity
        self.save()

    def save(self):
        """Помечаем сессию, как измененную"""
        self.session.modified = True  # при сохранении помечаем сессиию, как модифицированную

    def remove(self, product):
        """Удаление товара из корзины"""
        product_id = str(product.id)  # id товара преобразованный в строку для использования в словаре json
        if product_id in self.cart:  # если в словаре корзины товар с таким id существует
            del self.cart[product_id]  # удалить такой товар из словаря корзины
        self.save()  # вызываем метод сохранения

    def get_total_price(self):
        """Общая стоимость товаров"""
        return sum(Decimal(item['price']) * item['quantity'] for item in self.cart.values())

    def clear(self):
        """Очистка корзины"""
        del self.session[settings.CART_SESSION_ID]
        self.save()

    @property
    def coupon(self):
        """Cвойство класса через декоратор property"""
        if self.coupon_id:
            return Coupon.objects.get(id=self.coupon_id)
        return None

    def get_discount(self):
        """Размер скидки, если есть купон"""
        if self.coupon:
            return (self.coupon.discount / Decimal('100')) * self.get_total_price()
        return Decimal('0')

    def get_total_price_after_discount(self):
        """Общая стоимость товара с учетом купона"""
        return self.get_total_price() - self.get_discount()
